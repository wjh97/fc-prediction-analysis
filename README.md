### Resolution Handler Usage
Usage: resolution_handler.py [OPTIONS] PAIRFILE DESTDIR


### Resolution Handler Arguments
1. PAIRFILE is a file with the (sentinel-2, landsat) pairs to be processed. Look at "pairfile_example.txt" for reference/formatting.
2. DESTDIR is the directory for storing outputs.


### Resolution Handler Options
1. -m, memory for the batch job.
2. --nosubmit, don't submit batch job.
3. --nocleanup, keep all files.
4. --fgb_path, provide a path to a flat geobuff file if you want to sample the fractional cover outputs.


### To send only essential output files to the destination directory:
```sh
cd /export/home/halew/fractional_cover/fc-prediction-analysis/src && \
./resolution_handler.py \
/export/home/halew/fractional_cover/fc-prediction-analysis/extra/pairfile_example.txt \
/scratch/rsc8/halew/fractional_cover/processing \
--fgb_path /export/home/halew/fractional_cover/fc-prediction-analysis/extra/egsample.fgb
```


### To send all files (interim and essential) to the destination directory -> add the "nocleanup" option:
```sh
cd /export/home/halew/fractional_cover/fc-prediction-analysis/src && \
./resolution_handler.py \
/export/home/halew/fractional_cover/fc-prediction-analysis/extra/pairfile_example.txt \
/scratch/rsc8/halew/fractional_cover/processing \
--fgb_path /export/home/halew/fractional_cover/fc-prediction-analysis/extra/egsample.fgb \
--nocleanup
```


### To run without submitting to the batch queue -> add the "nosubmit" option:
```sh
cd /export/home/halew/fractional_cover/fc-prediction-analysis/src && \
./resolution_handler.py \
/export/home/halew/fractional_cover/fc-prediction-analysis/extra/pairfile_example.txt \
/scratch/rsc8/halew/fractional_cover/processing \
--nosubmit
```