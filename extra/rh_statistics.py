#!/usr/bin/env python


import click
import subprocess
import jq
import json
from pathlib import Path
from plotly.subplots import make_subplots
import plotly.graph_objects as go


RANGE = 256
PALETTE = {"AVGF": 'orangered', "PREDF": 'royalblue'}
TYPES = ["BARE", "GREEN", "DRY"]
NAMES = {
    0: ("PREDF", "predf_diff"),
    1: ("AVGF", "avgf_diff"),
}
X = list(range(RANGE))


def setup_dataframes_write_textfiles(n, bands, name, setups, targetdir):

    fout = open(targetdir.joinpath(f"{name}.txt"), 'w')

    for i in range(n):
        buckets = jq.compile(".").input_values(bands[i]['histogram']['buckets']).all()
        type = TYPES[i]
        setups[type][name] = buckets
        fout.write(f"{type}")
        for bucket in buckets:
            fout.write(f", {bucket}")
        fout.write("\n")
        i += 1


def generate_histograms(n, setups, targetdir, xmin, xmax):

    fig = make_subplots(
        rows=1,
        cols=3,
        subplot_titles=[
            'BARE',
            'GREEN',
            'DRY',
        ],
        shared_xaxes='rows',
        x_title = 'Pixel Value',
        y_title = 'Frequency'
    )

    fig.update_layout(height=1200)
    fig.update_layout(width=1800)
    fig.update_layout(xaxis=dict(range=[xmin, xmax]))
    fig.update_layout(showlegend=False)
    title_top = "BARE, GREEN, DRY Histograms\n"
    title_bottom = "key: Average First == RED, Predict First == BLUE"
    fig.update_layout(title=(title_top + title_bottom), title_x=0.5)

    row = 1
    for i in range(n):
        col = i+1
        type = TYPES[i]
        avgf_data = setups[type]["AVGF"]
        predf_data = setups[type]["PREDF"]
        fig.add_trace(
            go.Scatter(
                x=X,
                y=avgf_data,
                name=f"{type}_AVGF",
                line=dict(color='#DC3912')
            ),
            row=row, col=col
        )
        fig.add_trace(
            go.Scatter(
                x=X,
                y=predf_data,
                name=f"{type}_PREDF",
                line=dict(color='#3366CC')
            ),
            row=row, col=col
        )
    fig.write_image(targetdir.joinpath("BARE_GREEN_DRY_histograms.png"))


@click.command()
@click.argument("targetdir")
@click.option("--xmin", default=70,
              help="min x value for histograms")
@click.option("--xmax", default=125,
              help="max x value for histograms")
def mainRoutine(targetdir, xmin, xmax):

    targetdir = Path(targetdir)

    gdalinfo_commands = [
        'gdalinfo',
        '-noct',
        '-nomd',
        '-norat',
        '-hist',
        '-json',
    ]

    print("collecting RGB histogram data using gdalinfo...", flush=True)

    setups = {"BARE": dict(), "GREEN": dict(), "DRY": dict()}
    for index, info in NAMES.items():
        name = info[0]
        filename = info[1]
        full_path = targetdir.joinpath(f"{filename}.img")
        gdalinfo = gdalinfo_commands + [full_path]
        metadata_json = subprocess.run(gdalinfo, capture_output=True)
        loaded_metadata = json.loads(metadata_json.stdout)
        bands = jq.compile(".").input_values(loaded_metadata['bands']).all()
        n = len(bands)
        setup_dataframes_write_textfiles(n, bands, name, setups, targetdir)

    print(f"generating histograms in: {targetdir}", flush=True)
    generate_histograms(n, setups, targetdir, xmin, xmax)


if __name__ == "__main__":
    mainRoutine()