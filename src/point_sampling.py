#!/usr/bin/env python


import click
from pathlib import Path
import os
import geopandas as gpd
import rasterio
from shapely.geometry import box
import sys


# classifiers:
# landsat == lsat
# sentinel-2 average first (avgf) == s2a
# sentinel-2 predict first (predf) == s2p


NUM_RASTERS = 3


class SampleRegion:

    def __init__(self, images, dir, fgb_path):

        self.dir = dir

        if len(images) != NUM_RASTERS:
            sys.exit("There MUST be 3 target images: lsat, s2a, and s2p.")

        self.landsat = self.dir.joinpath(images[0])
        self.sen2_avgf = self.dir.joinpath(images[1])
        self.sen2_predf = self.dir.joinpath(images[2])
        self.classifiers = ("lsat", "s2a", "s2p")

        with rasterio.open(self.landsat) as lsat:
            lsat_crs = lsat.crs
            lsat_ext = box(*lsat.bounds)

        gdf = gpd.read_file(fgb_path)
        gdf = gdf.to_crs(lsat_crs)
        sr_ext = box(*gdf.total_bounds)
        intersection = sr_ext.intersection(lsat_ext)
        gdf = gdf.clip(intersection)
        self.gdf = gdf.reset_index()
        self.empty = not (sr_ext.intersects(lsat_ext))

        # set points list from dataframe column
        self.points = list()
        point_column = self.gdf['geometry']
        for i in self.gdf.index:
            self.points.append(Point(i, point_column[i]))

        self.samples = dict()
        for c in self.classifiers:
            for i in range(NUM_RASTERS):
                self.samples[f"{c}_{i}"] = list()

    def get_points(self):
        return self.points

    def get_gdf(self):
        return self.gdf

    def is_empty(self):
        return self.empty

    def attach_samples_to_gdf(self):
        self.gdf = self.gdf.join(gpd.GeoDataFrame(self.samples))

    def sample_images_all_points(self):

        for point in self.points:
            point.sample_image(self.landsat, "lsat", self.samples)
            point.sample_image(self.sen2_avgf, "s2a", self.samples)
            point.sample_image(self.sen2_predf, "s2p", self.samples)


class Point:

    def __init__(self, index, point):
        self.index = index
        self.point = point
        self.xy = (self.point.x, self.point.y)
        self.samples = list()

    def sample_image(self, image, classifier, samples):
        with rasterio.open(image) as src:
            for bgd in src.sample([self.xy]):
                sample = tuple(bgd.tolist())
                for i in range(NUM_RASTERS):
                    samples[f"{classifier}_{i}"].append(sample[i])
                self.samples.append(sample)

    def __str__(self):
        return f"{self.index}: {self.xy}, samples: {self.samples}"


# takes in a directory, finds the 3 target files,
# puts them in a list, and returns the list.
# target files: clipped landsat dp0,
# sen2 avgf, and sen2 predf.
def get_images(dir):
    images = dict()
    i = 0
    for root, dirs, files in os.walk(dir):
        for file in files:
            if "xml" in file:
                continue
            if "clipped" in file and "dp0" in file:
                images[0] = file
                i += 1
            if "avgf" in file and "aj0" in file:
                images[1] = file
                i += 1
            if "predf" in file and "aj0" in file:
                images[2] = file
                i += 1
    if i != 3:
        exit_message = """Failed to retrieve the three output images:
- landsat dp0 clipped
- sentinel-2 avgf
- sentinel-2 predf
"""
        sys.exit(exit_message)
    return images


@click.command()
@click.argument("targetdir")
@click.argument("fgb_path")
def mainRoutine(targetdir, fgb_path):

    dir = Path(targetdir)
    name = Path(os.path.basename(fgb_path)).with_suffix('')

    images = get_images(dir)
    sr = SampleRegion(images, dir, fgb_path)

    if sr.is_empty():
        print("Sample region does not intersect with images.")
        return 0

    sr.sample_images_all_points()
    sr.attach_samples_to_gdf()
    gdf = sr.get_gdf()

    shp_and_geo = dir.joinpath("shapefile_and_geojson")
    os.mkdir(shp_and_geo)

    shp_outfile = shp_and_geo.joinpath(f"{name}_sampled.shp")
    gdf.to_file(shp_outfile, driver="ESRI Shapefile")

    geojson_outfile = shp_and_geo.joinpath(f"{name}_sampled.geojson")
    gdf.to_file(geojson_outfile, driver='GeoJSON')
    return 0


if __name__ == "__main__":
    mainRoutine()
