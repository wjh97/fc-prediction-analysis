#!/usr/bin/env python


import rasterio
from shapely.geometry import box
import click
import json
from rasterio.mask import mask
import subprocess as sp


def get_features(gdf):
    return [json.loads(gdf.to_json())['features'][0]['geometry']]


def apply_mask(src_img, dest_img, intersection):

    src = rasterio.open(src_img)
    out = mask(src, [intersection], crop=True)
    out_img = out[0]
    out_transform = out[1]
    meta = src.meta.copy()
    meta_update = {"height": out_img.shape[1], "width": out_img.shape[2],
                   "transform": out_transform}
    meta.update(meta_update)

    with rasterio.open(dest_img, "w", **meta) as dest:
        dest.write(out_img)


def get_crs_and_extent(src_img, trgt_img):

    print("source image: ", src_img)
    print("target image: ", trgt_img)

    with rasterio.open(src_img) as src, rasterio.open(trgt_img) as trgt:

        src_crs = src.crs
        trgt_crs = trgt.crs

        print(f"{src_img} crs: ", src_crs)
        print(f"{trgt_img} crs: ", trgt_crs)

        src_ext = box(*src.bounds)
        trgt_ext = box(*trgt.bounds)

        print(f"{src_img} extents: ", src_ext)
        print(f"{trgt_img} extents: ", trgt_ext)

    return src_crs, trgt_crs, src_ext, trgt_ext


def warp(int_src_img, src_crs, trgt_crs):

    if src_crs == trgt_crs:

        print("NO reprojection needed...")
        int_src_reproj = int_src_img

    else:
        print("reprojection needed, computing reprojection...")
        int_src_reproj = f"{int_src_img.strip('.img')}_reprojected.img"
        warp_args = [
            "gdalwarp",
            "-t_srs",
            str(trgt_crs),
            int_src_img,
            int_src_reproj,
        ]
        print("reprojection args: ", warp_args)
        sp.run(warp_args)

    return int_src_reproj


@click.command()
@click.argument("int_src_img")
@click.argument("int_trgt_img")
@click.argument("clip_src_img")
@click.argument("clip_dest_img")
def mainRoutine(int_src_img, int_trgt_img, clip_src_img, clip_dest_img):

    src_crs, trgt_crs, src_ext, trgt_ext = get_crs_and_extent(int_src_img,
                                                              int_trgt_img)

    int_src_reproj = warp(int_src_img, src_crs, trgt_crs)

    print("transformed source image: ", int_src_reproj)

    with rasterio.open(int_src_reproj) as ist:
        src_ext_reproj = box(*ist.bounds)
        print("reprojected extents: ", src_ext_reproj)
        intersection = src_ext_reproj.intersection(trgt_ext)
        print("intersection: ", intersection)
        apply_mask(clip_src_img, clip_dest_img, intersection)


if __name__ == "__main__":
    mainRoutine()
