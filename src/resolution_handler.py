#!/usr/bin/env python


import click
from rsc import qvf
import rsc.qv as qv
import os
from pathlib import Path
import uuid

import sys
sys.path.append("/export/home/halew/batch_workflow_manager")
from batch_job_manager import Job, JobSequence

FC_PREFIX = "singularity exec $FRACTIONALCOVER3_PROD_IMAGE"
RF_PREFIX = "singularity exec $RAPIDFIRE_PROD_IMAGE"


def batch_format(prefix, script):
    script_path = Path(os.getcwd()).joinpath(script)
    return f"{prefix} python {str(script_path)}"


def change_zonecode_and_append(in_image, zonecode, method):
    change_zc = qvf.changezonecode(in_image, zonecode)
    out_image = f"{change_zc.removesuffix('.img')}_{method}r30m.img"
    return out_image


def send_to_dest_dir(file_list, destdir):
    secondary_string = ""
    for file in file_list:
        secondary_string += f"mv {file} {destdir}\n"
    return secondary_string


def init_job(k, destdir, memory):
    name = f"pair_{k}"
    pairdir = destdir.joinpath(name)
    os.mkdir(pairdir)
    job = Job(k, name, pairdir, memory, 2, 1)
    return job, pairdir


def generate_primary(inputs, interims, outputs, recall):

    warp_suffix = f"-t {outputs['LSAT_DP0_CLIPPED']} -r average"
    WARPLIKE_FULL = batch_format(FC_PREFIX, 'warplike.py')

    clip = f"""{inputs['SEN2']} \
{inputs['LSAT_DP0']} \
{inputs['LSAT_DP0']} \
{outputs['LSAT_DP0_CLIPPED']}"""

    primary_string = f"""
export GDAL_CACHE_MAX=1024
export GDAL_MAX_DATASET_POOL_SIZE=500
{FC_PREFIX} qv_recall {recall}
{batch_format(RF_PREFIX, 'clip.py')} {clip}
{WARPLIKE_FULL} -i {inputs["SEN2"]} \
-o {interims["SEN2_ABA_AVGF"]} {warp_suffix}
{WARPLIKE_FULL} -i {inputs["SEN2_ABB"]} \
-o {interims["SEN2_ABB_AVGF"]} {warp_suffix}
{WARPLIKE_FULL} -i {inputs["SEN2_AJ0"]} \
-o {outputs["SEN2_AJ0_PREDF"]} {warp_suffix}
{FC_PREFIX} qv_fractionalcover_sentinel2.py -i {interims["SEN2_ABA_AVGF"]}
"""
    return primary_string


def generate_secondary(outputs, pairdir, nocleanup, fgb_path):

    if nocleanup:
        secondary_string = f"cp -a . {pairdir}\n"
    else:
        secondary_string = send_to_dest_dir(list(outputs.values()), pairdir)

    if fgb_path is not None:
        PS = batch_format(RF_PREFIX, 'point_sampling.py')
        secondary_string += f"{PS} {str(pairdir)} {fgb_path}"

    return secondary_string


def make_inputs(sen2, lsat):
    inputs = {
        "SEN2": sen2,
        "SEN2_ABB": qvf.changestage(sen2, "abb"),
        "SEN2_AJ0": qvf.changestage(sen2, "aj0"),
        "LSAT_DP0": qvf.changestage(lsat, "dp0"),
    }
    return inputs


def make_interims_and_outputs(inputs):

    zc = qvf.zonecode(inputs["LSAT_DP0"])

    interims = {
        "SEN2_ABA_AVGF": change_zonecode_and_append(inputs["SEN2"],
                                                    zc, "avgf"),
        "SEN2_ABB_AVGF": change_zonecode_and_append(inputs["SEN2_ABB"],
                                                    zc, "avgf"),
    }

    outputs = {
        "SEN2_AJ0_AVGF": qvf.changestage(interims["SEN2_ABA_AVGF"], "aj0"),
        "SEN2_AJ0_PREDF": change_zonecode_and_append(inputs["SEN2_AJ0"],
                                                     zc, "predf"),
    }
    return interims, outputs


def make_job_for_pair(k, sen2, lsat, destdir, memory,
                      nosubmit, nocleanup, fgb_path):

    job, pairdir = init_job(k, destdir, memory)

    print(f"image pair provided: ({sen2}, {lsat})")
    print("outputs will be stored in: ", pairdir)

    inputs = make_inputs(sen2, lsat)

    recall = ""
    print("Dmgetting the input files...")
    for name, input in inputs.items():
        print(f"{name}: {input}")
        recall += f"{input} "
    qv.doDmget(list(inputs.values()))

    interims, outputs = make_interims_and_outputs(inputs)

    # landsat dp0 will get clipped to the intersection bounds
    bare = inputs["LSAT_DP0"].strip('.img')
    outputs["LSAT_DP0_CLIPPED"] = f"{bare}_clipped.img"

    secondary_string = generate_secondary(outputs, pairdir,
                                          nocleanup, fgb_path)
    primary_string = generate_primary(inputs, interims, outputs, recall)
    full_command_string = primary_string + secondary_string
    job.set_bash_commands(full_command_string)
    return job


@click.command()
@click.argument("pairfile")
@click.argument("destdir")
@click.option("-m", "--memory", default=4,
              help="memory allocated to each batch job")
@click.option("--nosubmit", is_flag=True,
              help="don't submit batch job")
@click.option("--nocleanup", is_flag=True,
              help="interim files are kept in destdir")
@click.option("--fgb_path", default=None,
              help="flat geobuff point sampling file path")
def mainRoutine(pairfile, destdir, memory, nosubmit, nocleanup, fgb_path):

    destdir = Path(destdir)
    code = f'{uuid.uuid4().time_hi_version:X}'
    destdir = destdir.joinpath(f"resolution_handler_{code}")
    os.mkdir(destdir)

    js = JobSequence()
    js.request_flash()
    f = open(pairfile, "r")
    k = 0
    for line in f:
        scenes = line.split(',')
        sen2_image = scenes[0]
        landsat_image = scenes[1].strip('\n')
        job = make_job_for_pair(k, sen2_image, landsat_image, destdir, memory,
                                nosubmit, nocleanup, fgb_path)
        js.add_job(job)
        k += 1

    if not nosubmit:

        js.write_shell_scripts()
        js.submit_jobs()
        js.wait_on_jobs()

    else:
        print("no jobs were submitted")
    return


if __name__ == "__main__":
    mainRoutine()
