#!/usr/bin/env python
"""
Reproject a given input image to the projection, extent and pixel grid of the given
template image. 

Also has a tacked-on adaptation to accept an explicit projection instead of
a templatefile, in which case it tries to work out the grid as best it can. 

"""

import sys
import os
import optparse
import tempfile

from osgeo import gdal
from osgeo import osr

from rios import calcstats
from rios import rat
from rios import fileinfo
from rsc.utils import gdalcommon
from rsc.utils import DNscaling
from rsc.utils import history


def makeProjFile(projWkt):
    """
    Make a temporary file of the template projection
    Return the filename
    
    """
    (fd, projFile) = tempfile.mkstemp(dir='.', prefix='tmp', suffix='.prj')
    os.close(fd)
    
    f = open(projFile, 'w')
    fixedWkt = gdalcommon.fixDodgyImagineSpheroid(projWkt)
    f.write(fixedWkt)
    f.close()
    return projFile


class FakeInfo(object):
    "A fake gdalcommon.info object, for use with --projection"
    pass


def reprojCorners(tr, sourceInfo, templateInfo):
    """
    Reproject all corners of the given sourceInfo, using the given 
    transformation, then store the resulting bounds on the given
    templateInfo object. 
    """
    (tlX, tlY, z) = tr.TransformPoint(sourceInfo.xMin, sourceInfo.yMax)
    (trX, trY, z) = tr.TransformPoint(sourceInfo.xMax, sourceInfo.yMax)
    (blX, blY, z) = tr.TransformPoint(sourceInfo.xMin, sourceInfo.yMin)
    (brX, brY, z) = tr.TransformPoint(sourceInfo.xMax, sourceInfo.yMin)
    templateInfo.xMin = min(tlX, trX, blX, brX)
    templateInfo.yMin = min(tlY, trY, blY, brY)
    templateInfo.xMax = max(tlX, trX, blX, brX)
    templateInfo.yMax = max(tlY, trY, blY, brY)
    templateInfo.xRes = sourceInfo.xRes
    templateInfo.yRes = sourceInfo.yRes


def snapValue(value, res):
    """
    Snaps the given value to be a multiple of the
    given resolution
    """
    n = int(value / res)
    snapped = n * res
    return snapped
    

def alignGrid(sourceInfo, templateInfo):
    """
    Align the templateInfo grid such that its alignment characteristics 
    match those of the sourceInfo. 
    """
    xOffset = sourceInfo.xMin - snapValue(sourceInfo.xMin, sourceInfo.xRes)
    yOffset = sourceInfo.yMin - snapValue(sourceInfo.yMin, sourceInfo.yRes)
    
    templateInfo.xMin = snapValue(templateInfo.xMin, templateInfo.xRes) + xOffset
    templateInfo.xMax = snapValue(templateInfo.xMax, templateInfo.xRes) + xOffset
    templateInfo.yMin = snapValue(templateInfo.yMin, templateInfo.yRes) + yOffset
    templateInfo.yMax = snapValue(templateInfo.yMax, templateInfo.yRes) + yOffset
    

def deduceOutputInfo(cmdargs, sourceInfo):
    """
    Adaption so that it is possible to specify the output projection
    instead of a templatefile. 
    """
    srOut = None
    templateInfo = FakeInfo()
    if cmdargs.projection.startswith('EPSG:'):
        epsg = int(cmdargs.projection.split(':')[1])
        srOut = osr.SpatialReference()
        srOut.ImportFromEPSG(epsg)
        templateInfo.projection = srOut.ExportToWkt()
    elif os.path.exists(cmdargs.projection):
        templateInfo.projection = open(cmdargs.projection).read()
    else:
        templateInfo.projection = cmdargs.projection
    if srOut is None:
        srOut = osr.SpatialReference(wkt=templateInfo.projection)
    
    srIn = osr.SpatialReference(wkt=sourceInfo.projection)

    fileinfo.preventGdal3axisSwap(srIn)
    fileinfo.preventGdal3axisSwap(srOut)

    tr = osr.CoordinateTransformation(srIn, srOut)
    
    # Reproject the corners to the output projection
    reprojCorners(tr, sourceInfo, templateInfo)
    
    alignGrid(sourceInfo, templateInfo)
    return templateInfo
    
    
def adjustTemplateExtents(sourceInfo, templateInfo):
    """
    Adjust the extents of the template so they match the 
    reprojected extents of the source, but are on the template 
    grid. Update the templateInfo extents in place.
    
    """
    tmpInfo = FakeInfo() # temporary info object.
    tmpInfo.projection = templateInfo.projection

    sr1 = osr.SpatialReference(wkt=sourceInfo.projection)
    sr2 = osr.SpatialReference(wkt=tmpInfo.projection)

    fileinfo.preventGdal3axisSwap(sr1)
    fileinfo.preventGdal3axisSwap(sr2)

    tr = osr.CoordinateTransformation(sr1, sr2)
    reprojCorners(tr, sourceInfo, tmpInfo)
    alignGrid(templateInfo, tmpInfo)
    templateInfo.xMin = tmpInfo.xMin
    templateInfo.xMax = tmpInfo.xMax
    templateInfo.yMin = tmpInfo.yMin
    templateInfo.yMax = tmpInfo.yMax


def doReproject(cmdargs):
    """
    Do the reprojection
    
    """
    sourceInfo = gdalcommon.info(cmdargs.infile)
    srcProjFile = makeProjFile(sourceInfo.projection)
    if cmdargs.templatefile is not None:
        templateInfo = gdalcommon.info(cmdargs.templatefile)
        if cmdargs.noclip:
            adjustTemplateExtents(sourceInfo, templateInfo)
    elif cmdargs.projection is not None:
        templateInfo = deduceOutputInfo(cmdargs, sourceInfo)
    tgtProjFile = makeProjFile(templateInfo.projection)
    
    cmdTemplate = "gdalwarp %s %s -t_srs %s -s_srs %s -te %s -tr %s %s -r %s %s %s"
    
    ovrSwitch = ""
    if int(gdal.__version__.split(".")[0]) >= 2:
        ovrSwitch = "-ovr NONE"

    gdalCmdOptions = gdalcommon.getOptionsForCmdLine(cmdargs.gdaldriver)
    extentString = "%s %s %s %s" % (templateInfo.xMin, templateInfo.yMin, templateInfo.xMax, templateInfo.yMax)
    resolutionString = "%s %s" % (templateInfo.xRes, templateInfo.yRes)
    nullVal = gdalcommon.getNoDataValueFromFile(cmdargs.infile)
    if nullVal is not None:
        nullHandlingStr = "-srcnodata %s -dstnodata %s" % (nullVal, nullVal)
    else:
        nullHandlingStr = ""
    
    cmd = cmdTemplate % (ovrSwitch, gdalCmdOptions, tgtProjFile, srcProjFile, 
        extentString, resolutionString, nullHandlingStr, cmdargs.resamplemethod, 
        cmdargs.infile, cmdargs.outfile)
    os.system(cmd)
    
    for projFile in [tgtProjFile, srcProjFile]:
        os.remove(projFile)

    # Copy the attribute table across.
    copyAttributes(cmdargs.infile, cmdargs.outfile)
    
    # Calculate statistics
    ds = gdal.Open(cmdargs.outfile, gdal.GA_Update)
    calcstats.calcStats(ds)
    del ds
    return cmd


def copyAttributes(srcfile, dstfile):
    """
    Copy the attributes from the source image to the
    destination image. The histogram column is not copied.
    If there is no attribute table in srcfile then this
    function has not effect on dstfile.

    """
    sourceInfo = gdalcommon.info(cmdargs.infile)
    ds = gdal.Open(srcfile)
    for bandnum in range(1, sourceInfo.rasterCount+1):
        band = ds.GetRasterBand(bandnum)
        # Check if the table exists then copy the attributes.
        if band.GetDefaultRAT():
            col_names = rat.getColumnNames(srcfile, bandNumber=bandnum)
            for col_name in col_names:
                col_usage = rat.getUsageOfColumn(srcfile, col_name, bandNumber=bandnum)
                # Don't copy the histogram column as the stats are different
                if col_usage != gdal.GFU_PixelCount:
                    col_data = rat.readColumn(srcfile, col_name, bandNumber=bandnum)
                    col_type = rat.inferColumnType(col_data)
                    rat.writeColumn(
                        dstfile, col_name, col_data, col_type,
                        bandNumber=bandnum, colUsage=col_usage)
    del ds


def addHistory(cmdargs, warpCmd):
    """
    Add file history to output file
    
    """
    opt = {}
    opt['DESCRIPTION'] = """
        Created by reprojecting to match a template file, using gdalwarp. Any occurrences
        of the dodgy Imagine GDA94 spheroid parameters were fixed before reprojection. 
    """
    opt['GDALWARP_CMD'] = warpCmd
    
    parents = [cmdargs.infile]
    if cmdargs.templatefile is not None:
        parents.append(cmdargs.templatefile)
    history.insertMetadataFilename(cmdargs.outfile, parents, opt)
    

def doAll(cmdargs):
    """
    Reproject an image to match another image
    
    """
    warpCmd = doReproject(cmdargs)
    if DNscaling.hasDNscaling(cmdargs.infile):
        DNscaling.copyDNscaling(cmdargs.infile, cmdargs.outfile)
    addHistory(cmdargs, warpCmd)


class CmdArgs:
    def __init__(self):
        p = optparse.OptionParser("%prog [options]\n\nWarp an image to be like another image\n"+
            "If --projection is given instead of --templatefile, then just\nreproject to that, with "+
            "grid alignment matching existing as well as possible")
        p.add_option("-i", "--infile", dest="infile", help="Image to reproject")
        p.add_option("-t", "--templatefile", dest="templatefile", help="Image to use as template")
        p.add_option("-p", "--projection", dest="projection", 
            help="Projection to use for output (if no templatefile given). Can be of the form "+
            "'EPSG:nnnnn', 'WKT string', or a filename containing a WKT string. ")
        p.add_option("-o", "--outfile", dest="outfile", help="Name of output file")
        p.add_option("-r", "--resamplemethod", dest="resamplemethod", default="near", 
            help=("Resample method, as per gdalwarp, options are " +
                "{near, bilinear, cubic, cubicspline, lanczos} (default=%default)"))
        p.add_option("-d", "--gdaldriver", dest="gdaldriver", default="HFA", help="Name of GDAL output driver")
        p.add_option("-n", "--noclip", dest="noclip", default=False, action="store_true",
            help="Do not clip infile to the extents of templatefile - i.e. align the grids only.")
        (options, args) = p.parse_args()
        self.__dict__.update(options.__dict__)
        
        if self.infile is None or (self.templatefile is None and self.projection is None):
            p.print_help()
            sys.exit()

if __name__ == "__main__":
    cmdargs = CmdArgs()
    doAll(cmdargs)
