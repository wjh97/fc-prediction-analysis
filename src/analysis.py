#!/usr/bin/env python


import click
import pandas as pd
import geopandas as gpd


NUM_BANDS = 3
NO_DATA = 255
BANDS = {0: "BARE", 1: "GREEN", 2: "DRY"}
TYPES = ["s2a", "s2p"]


class BandStats:

    def __init__(self, gdf, band, type):

        self.band = band
        self.type = type
        if type == "s2a":
            self.name = "Average First"
        elif type == "s2p":
            self.name = "Prediction First"
        self.series = pd.Series(gdf[f"{type}_{self.band}"])
        self.lsat_series = pd.Series(gdf[f"lsat_{self.band}"])

    def drop_index(self, i):
        self.series.drop(i, inplace=True)
        self.lsat_series.drop(i, inplace=True)

    # run for all instances before "make_diff"
    def remove_rows(self):

        for i, pixel in self.series.items():
            if pixel == NO_DATA:
                self.drop_index(i)

        for i, pixel in self.lsat_series.items():
            if pixel == NO_DATA:
                self.drop_index(i)

        # self.series.reset_index(drop=True, inplace=True)
        # self.lsat_series.reset_index(drop=True, inplace=True)

    def make_diff(self):
        self.diff = self.lsat_series.subtract(self.series)

    def print_diff(self):
        print(self.diff)

    def make_minmax(self):
        min = self.diff.min()
        max = self.diff.max()
        min_indices = set()
        max_indices = set()
        minmax = dict()
        for i, value in self.diff.items():
            if value == min:
                min_indices.add(i)
            elif value == max:
                max_indices.add(i)
        minmax['negmax'] = (min, min_indices)
        minmax['posmax'] = (max, max_indices)
        self.minmax = minmax

    def compute_mean(self):
        self.mean = self.diff.mean()

    def __str__(self):
        negmax = self.minmax['negmax']
        posmax = self.minmax['posmax']
        out = f"""
Band: {BANDS[self.band]}
Type of transformation: {self.name}
Difference: lsat subtract sen2
Mean difference: {self.mean}
Highest negative difference: {negmax[0]}
Occurs for the following sample indexes: {negmax[1]}
Highest positive difference: {posmax[0]}
Occurs for the following sample indexes: {posmax[1]}
"""
        print(out)


@click.command()
@click.argument("geojson_path")
def mainRoutine(geojson_path):

    gdf = gpd.read_file(geojson_path)

    band_stats = list()
    for i in range(2):
        band_stats.append(BandStats(gdf, 0, TYPES[i]))
        band_stats.append(BandStats(gdf, 1, TYPES[i]))
        band_stats.append(BandStats(gdf, 2, TYPES[i]))

    for bs in band_stats:
        bs.remove_rows()
        bs.make_diff()
        bs.make_minmax()
        bs.compute_mean()
        bs.__str__()
    return 0


if __name__ == "__main__":
    mainRoutine()
